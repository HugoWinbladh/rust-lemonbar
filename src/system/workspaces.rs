use i3ipc::I3Connection;

pub fn load() -> i3ipc::reply::Workspaces{
    let mut connection = I3Connection::connect().unwrap();

    connection.get_workspaces().unwrap()
}


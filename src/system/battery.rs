use crate::system::command;

#[derive(Debug)]
pub struct Battery {
    pub percentage: String,
    pub charging: bool,
    pub logo: String,
}

pub fn load() -> Battery {
    let percent = command::run(String::from("acpi"), &[], String::from(r"\d*%"));
    let tmp = command::run(String::from("acpi"), &[], String::from(r"Discharging"));
    let charging: bool = match tmp.as_ref() {
        "" => true,
        _ => false,
    };
    let logo: String = match percent[..percent.len()-1].parse().unwrap() {
        80 ... 100 => String::from("\u{f240}"),
        60 ... 80 => String::from("\u{f241}"),
        40 ... 60 => String::from("\u{f242}"),
        20 ... 40 => String::from("\u{f243}"),
        0 ... 20 => String::from("\u{f244}"),
        _ => String::from(""),
    };
    let battery = Battery {
        percentage: percent,
        charging: charging,
        logo: logo
    };
    battery
}

pub mod command;
pub mod battery;
pub mod disk;
pub mod network;
pub mod time;
pub mod volume;
pub mod workspaces;

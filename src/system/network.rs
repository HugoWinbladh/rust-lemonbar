use crate::system::command;

#[derive(Debug)]
pub enum Conn {
    Ethernet,
    Wireless,
}

#[derive(Debug)]
pub struct Network {
    pub ip: String,
    pub connected: bool,
    pub connection: Conn,
}

pub fn load() -> Network {
    let ip = ip();
    let connected = if ip == "" { false } else { true };
    let connection = Conn::Wireless;
    let network = Network {
        ip,
        connected,
        connection
    };
    network
}

fn ip() -> String {
    let result = command::run(String::from("ip"), &[String::from("addr"), String::from("show"), String::from("wlo1"), String::from("primary")], String::from(r"inet.\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}"));
    String::from(&result[5..])
}

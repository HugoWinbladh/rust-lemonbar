use crate::system::command;

#[derive(Debug)]
pub struct Disk {
    pub used: String,
}

pub fn load() -> Disk {
    let used =  command::run(String::from("df"), &[String::from("-h"), String::from("/dev/sda5")], String::from(r"\d.%"));
    Disk { used }
}

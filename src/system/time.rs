use crate::system::command;

#[derive(Debug)]
pub struct Time {
    pub datetime: String,
}

pub fn load() -> Time {
    let datetime = command::run(String::from("date"), &[String::from("+%a, %h %d at %H:%M")], String::from(r".*"));
    Time { datetime }
}


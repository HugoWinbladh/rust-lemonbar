extern crate regex;

use regex::Regex;
use std::process::Command;


pub fn run(com: String, args: &[String], regex_string: String) -> String {
    let command = Command::new(&com)
        .args(args)
        .output()
        .expect("command failed").stdout;
    let output = std::str::from_utf8(&command).expect("failed converting to string");
    let re = Regex::new(&regex_string).unwrap();
    match re.captures(output) {
        Some(x) =>
            return String::from(x.get(0).unwrap().as_str()),
        None =>
            return String::from("error"),
    };
}


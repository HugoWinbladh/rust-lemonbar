use crate::system::command;
use regex::Regex;

#[derive(Debug)]
pub struct Volume {
    pub percentage: String,
    pub icon: String,
}

pub fn load() -> Volume {
    let result = command::run(String::from("pactl"), &[String::from("list"), String::from("sinks")], String::from(r"front-left:.\d{1,6}.*\d*%"));
    let re = Regex::new(&r"\d{1,3}%").unwrap();
    let percentage = match re.captures(result.as_ref()) {
        Some(x) =>
            String::from(x.get(0).unwrap().as_str()),
        None =>
            String::from("error"),
    };
    
    let icon: String = match percentage[..percentage.len()-1].parse().unwrap() {
        51 ... 100 => String::from("\u{f028}"),
        1 ... 50 => String::from("\u{f027}"),
        0 => String::from("\u{f026}"),
        _ => String::from("\u{f028}"),
    };
    Volume { percentage, icon}
}

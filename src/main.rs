//extern crate systemstat;

//use systemstat::{System, Platform};
use std::time::Duration;
use std::sync::{Arc, Mutex};
use std::thread;
use std::sync::mpsc::channel;
use i3ipc::{I3EventListener, Subscription, event::Event};

mod system;

const COLOR_0: &str = "000000";
const COLOR_1: &str = "BF616A";
const COLOR_2: &str = "D08770";
const COLOR_3: &str = "EBCB8B";
const COLOR_4: &str = "A3BE8C";
const COLOR_5: &str = "B48EAD";

struct Bar {
    battery: system::battery::Battery,
    network: system::network::Network,
    disk: system::disk::Disk,
    volume: system::volume::Volume,
    time: system::time::Time,
    workspaces: i3ipc::reply::Workspaces,
}

impl Bar {
    fn get_battery(&self) -> &system::battery::Battery {
        &self.battery
    }
    fn get_network(&self) -> &system::network::Network {
        &self.network
    }
    fn get_disk(&self) -> &system::disk::Disk {
        &self.disk
    }
    fn get_volume(&self) -> &system::volume::Volume {
        &self.volume
    }
    fn get_time(&self) -> &system::time::Time {
        &self.time
    }
    fn get_workspaces(&self) -> &i3ipc::reply::Workspaces {
        &self.workspaces
    }
    fn set_battery(&mut self, battery: system::battery::Battery) {
        self.battery = battery;
    }
    fn set_network(&mut self, network: system::network::Network) {
        self.network = network;
    }
    fn set_disk(&mut self, disk: system::disk::Disk) {
        self.disk = disk;
    }
    fn set_volume(&mut self, volume: system::volume::Volume) {
        self.volume = volume;
    }
    fn set_time(&mut self, time: system::time::Time) {
        self.time = time;
    }
    fn set_workspaces(&mut self, workspaces: i3ipc::reply::Workspaces) {
        self.workspaces = workspaces;
    }
}

fn main() {
    let battery = system::battery::load();
    let network = system::network::load();
    let disk = system::disk::load();
    let volume = system::volume::load();
    let time = system::time::load();
    let workspaces = system::workspaces::load();
    let bar = Arc::new(Mutex::new(Bar { battery, network, disk, volume, time, workspaces }));

    let (tx, rx) = channel();
    let (mut bar_clone, tx_clone) = (Arc::clone(&bar), tx.clone());
    thread::spawn(move || {
        listen(&mut bar_clone, tx_clone);
    });
    loop {
        let (mut bar_clone, tx_clone) = (Arc::clone(&bar), tx.clone());
        update(&mut bar_clone, tx_clone);
        rx.recv().unwrap();
        {
            let bar = bar.lock().unwrap();
            //println!("{:?}\n{:?}\n{:?}\n{:?}\n{:?}\n{:?}", bar.get_battery(), bar.get_network(), bar.get_disk(), bar.get_volume(), bar.get_time(), bar.get_workspaces());
            print(&bar);
        }
        //let mut fmt_str = format!();
        //info.hdd_usage = hdd(sys);
        //println!("{:?}\n{:?}\n{:?}\n{:?}\n{:?}\n{:?}", battery, network, disk, volume, time, workspaces);
        thread::sleep(Duration::from_millis(1000));
    }
}

fn update(bar: &mut Arc<Mutex<Bar>>, tx: std::sync::mpsc::Sender<()>) {
    let mut bar = bar.lock().unwrap();
    bar.set_battery(system::battery::load());
    bar.set_network(system::network::load());
    bar.set_disk(system::disk::load());
    bar.set_time(system::time::load());
    //bar.set_workspaces(system::workspaces::load());
    //let mut fmt_str = format!();
    //info.hdd_usage = hdd(sys);
    tx.send(()).unwrap();
}

fn listen(bar: &mut Arc<Mutex<Bar>>, tx: std::sync::mpsc::Sender<()>) {
    let mut listener = I3EventListener::connect().unwrap();

    let subs = [Subscription::Binding];
    listener.subscribe(&subs).unwrap();

    for event in listener.listen() {
        match event.unwrap() {
            Event::BindingEvent(e) => {
                let mut bar = bar.lock().unwrap();
                match &e.binding.command.as_str() {
                    c if c.contains("workspace") => {
                        bar.set_workspaces(system::workspaces::load());
                        print(&bar);
                    },
                    c if c.contains("pactl") => {
                        thread::sleep(Duration::from_millis(15));
                        bar.set_volume(system::volume::load());
                        print(&bar);
                    },
                    _ => ()
                }
                tx.send(()).unwrap();
            }
            _ => unreachable!()
        }
    }
}

fn print(bar: &Bar) {
    let mut workspaces: String = String::from("");
    for ws in bar.get_workspaces().workspaces.iter() {
        let focus_color_before = if ws.focused {
            &"%{B#FF444444}"
        } else {
            &""
        };
        let focus_color_after = if ws.focused {
            &"%{B-}"
        } else {
            &""
        };
        workspaces.push_str(&format!("{}  {}  {}", focus_color_before, &ws.name[ws.name.len()-3..], focus_color_after));
    }
    let ip = if bar.get_network().connected {
        bar.get_network().ip.clone()
    } else {
        "Disconnected".to_string().clone()
    };
    println!(
        "%{{l}}{} %{{r}}%{{F#FF{}}}%{{B#FF{}}}   \u{f0a0}  {}  %{{B#FF{}}}   {}  {}   %{{B#FF{}}}   \u{f1eb}  {}   %{{B#FF{}}}   {}  {}   %{{B#FF{}}}   {}   %{{F-}}%{{B-}}",
        workspaces,
        COLOR_0,
        COLOR_1,
        bar.get_disk().used,
        COLOR_2,
        bar.get_volume().icon,
        bar.get_volume().percentage,
        COLOR_3,
        ip,
        COLOR_4,
        bar.get_battery().logo,
        bar.get_battery().percentage,
        COLOR_5,
        bar.get_time().datetime
    );
    //println!("{:?}", bar.get_workspaces());

}
